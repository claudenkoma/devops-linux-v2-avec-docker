#FROM node:18-alpine
FROM node:10 AS builder

#creer repertoire pour notre application
WORKDIR /app
#regrouper les sources de l'application
COPY . .